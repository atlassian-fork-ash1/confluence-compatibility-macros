package com.atlassian.confluence.plugins.macros.compatibility;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
/**
 * Print the last 'n' blog posts.
 *
 * By default, it'll render the entire content of each blog post. You can change this
 * behaviour with "display=titles" or "display=excerpts"
 */
public class BlogPostsMacro extends BaseMacro implements Macro
{
    public static final Logger log = Logger.getLogger(BlogPostsMacro.class);
    public static final String DISPLAY_TITLES_ONLY = "titles";
    public static final String DISPLAY_EXCERPTS = "excerpts";

    private PageManager pageManager;
    private ExcerptHelper excerptHelper;
    private XhtmlContent xhtmlContent;
    private PermissionManager permissionManager;
    public static final ThreadLocal<Boolean> renderingThreadLocal = new ThreadLocal<Boolean>();

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
    	this.excerptHelper = excerptHelper;
    }
    
    public static class PostHtmlTuple
    {
        private PermissionManager permissionManager;
        private BlogPost post;
        private String renderedHtml;

        public PostHtmlTuple(PermissionManager permissionManager, BlogPost post, String renderedHtml)
        {
            this.permissionManager = permissionManager;
            this.post = post;
            this.renderedHtml = renderedHtml;
        }

        public BlogPost getPost()
        {
            return post;
        }

        public String getRenderedHtml()
        {
            return renderedHtml;
        }

        public boolean isEditable()
        {
            return permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.EDIT, post);
        }
    }

	
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException 
	{
		try
    	{
    		return execute(parameters, body, new DefaultConversionContext(renderContext));
    	}
    	catch(MacroExecutionException e)
    	{
    		throw new MacroException(e);
    	}
	}
	
	public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException 
	{
        if (isBeingRendered())
        {
        	return null;
        }

        String contentType = parameters.get("content");

        boolean popRequired = false;

        try
        {
        	setBeingRendered(true);

            ContentEntityObject ceo = conversionContext.getEntity();
            if (ceo instanceof AbstractPage)
            {
                AbstractPage abstractPage = (AbstractPage) ceo;

                if (ContentIncludeStack.contains(abstractPage))
                    return errorContent(
                            getText(
                                    "blogpostmacro.error.alreadyincludedpage",
                                    new Object[] { StringEscapeUtils.escapeHtml(abstractPage.getTitle()) }
                            )
                    );

                ContentIncludeStack.push(abstractPage);
                popRequired = true;
            }

            int maxPosts = getMaxPostCount(parameters);
            Date timeSince = getTimeSince(parameters);
            if (maxPosts == 0 && timeSince == null)
                maxPosts = 15;

            List blogPosts = pageManager.getRecentlyAddedBlogPosts(maxPosts, timeSince, conversionContext.getSpaceKey());
            Map<String, Object> contextMap = getMacroVelocityContext();
            contextMap.put("posts", toPostHtmlTuple(blogPosts, contentType));
            contextMap.put("contentType", contentType);
            if (DISPLAY_TITLES_ONLY.equals(contentType))
                return renderBlogTitles(contextMap);
            else
                return renderBlogs(contextMap);
        }
        catch (Exception e)
        {
            log.error("Error while trying to draw the last-n pages!", e);
            throw new MacroExecutionException(e);
        }
        finally
        {
        	setBeingRendered(false);
            if (popRequired)
                ContentIncludeStack.pop();
        }
	}
	

    ///CLOVER:OFF
    protected String getText(String key, Object[] substitution)
    {
        return ConfluenceActionSupport.getTextStatic(key, substitution);
    }

    protected Map<String, Object> getMacroVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }

    protected String renderBlogs(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate("templates/macros/lastblogs.vm", contextMap);
    }

    protected String renderBlogTitles(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate("templates/macros/lastblogtitles.vm", contextMap);
    }
    ///CLOVER:ON

    private Date getTimeSince(Map macroParameters) throws InvalidDurationException
    {
        Date timeSince = null;
        String timeSinceStr = macroParameters.get("time") == null ? null : macroParameters.get("time").toString();;
        if (TextUtils.stringSet(timeSinceStr))
        {
            long duration = DateUtils.getDuration(timeSinceStr);
            timeSince = new Date(new Date().getTime() - duration * 1000);
        }

        return timeSince;
    }

    private int getMaxPostCount(Map macroParameters)
    {
        String maxPostsStr = macroParameters.get("max") == null ? null : macroParameters.get("max").toString();

        if (TextUtils.stringSet(maxPostsStr))
        {
            try
            {
                return Integer.parseInt(maxPostsStr);
            }
            catch (NumberFormatException e)
            {
            }
        }

        return 0;
    }

    public List toPostHtmlTuple(List blogPosts, String contentType) throws XMLStreamException, XhtmlException
    {
        List list = new ArrayList(blogPosts.size());

        for (Iterator it = blogPosts.iterator(); it.hasNext();)
        {
            BlogPost post = (BlogPost) it.next();
//            String renderedHtml = wikiStyleRenderer.convertWikiToXHtml(post.toPageContext(), getContent(post, contentType));
            String renderedHtml = getContent(post, contentType);
            
            // extract error text only
            Pattern htmlErrorString = Pattern.compile("<div class=\"error\">.*</div>");
            Matcher m = htmlErrorString.matcher(renderedHtml);
            boolean hasError = m.find();
            if (hasError)
            {
                list.add(new PostHtmlTuple(permissionManager, post, m.group(0)));
            }
            else
            {
                list.add(new PostHtmlTuple(permissionManager, post, renderedHtml));
            }
        }

        return list;
    }

    protected String getContent(BlogPost post, String contentType) throws XMLStreamException, XhtmlException
    {
        if (DISPLAY_TITLES_ONLY.equals(contentType))
            return StringUtils.EMPTY;

        if (DISPLAY_EXCERPTS.equals(contentType))
        {
            String excerpt = excerptHelper.getExcerpt(post);
            if (!TextUtils.stringSet(excerpt))
            {
                excerpt = xhtmlContent.convertStorageToView(post.getBodyAsString(), new DefaultConversionContext(post.toPageContext()));
                if (excerpt.length() > 500)
                    excerpt = new StringBuffer(TextUtils.trimToEndingChar(excerpt, 500)).append("...").toString();
            }

            return excerpt;
        }

        return post.getBodyAsString();
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

	public RenderMode getBodyRenderMode() 
	{
		return RenderMode.NO_RENDER;
	}

	public boolean hasBody() 
	{
		return true;
	}
	
	 private String errorContent(String message)
	 {
		 return "<div class=\"error\">" + message + "</div>";
	 }

	public BodyType getBodyType() 
	{
		return BodyType.NONE;
	}

	public OutputType getOutputType() 
	{
		return OutputType.BLOCK;
	}
	
	@Override
	public TokenType getTokenType(Map parameters, String body, RenderContext context) 
	{
		return TokenType.BLOCK;
	}

	public void setXhtmlContent(XhtmlContent xhtmlContent)
	{
		this.xhtmlContent = xhtmlContent;
	}

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    protected boolean isBeingRendered()
    {
        final Boolean rendered = renderingThreadLocal.get();
        return null != rendered && rendered;
    }

	protected void setBeingRendered(final boolean beingRendered) 
    {
        renderingThreadLocal.set(beingRendered);
    }
}

package com.atlassian.confluence.plugins.macros.compatibility;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.links.WebLink;
import org.radeox.util.Encoder;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkRenderer;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.links.UrlLink;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;

/**
 * A simple macro to make hyperlinks
 */
public class LinkMacro extends BaseMacro implements Macro
{
    
    private LinkRenderer linkRenderer;

    public void setLinkRenderer(LinkRenderer linkRenderer)
    {
    	this.linkRenderer = linkRenderer;
    }
   
    ///CLOVER:OFF
    protected String getText(String key)
    {
        return ConfluenceActionSupport.getTextStatic(key);
    }
    ///CLOVER:ON
    
    public String execute(Map parameters, String body, RenderContext renderContext) 
    {
    	return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public String execute(Map<String, String> macroParams, String body, ConversionContext conversionContext)
    {
        String text = macroParams.get("text") != null? macroParams.get("text") : null;
        String url = macroParams.get("url") != null? macroParams.get("url") : null;
        RenderContext renderContext = conversionContext.getEntity().toPageContext();

        if (text == null && url == null)
        {
            text = macroParams.get("0");
            url = macroParams.get("1");
        }
        else if (url == null)
        {
            url = getEither(macroParams);
        }
        else if (text == null)
        {
            text = getEither(macroParams);
        }

        // check for single url argument (text == url)
        if (StringUtils.isNotBlank(text) && StringUtils.isBlank(url))
        {
        	url = text;
            text = Encoder.toEntity(url.charAt(0)) + url.substring(1);
        }

        if (text == null)
        {
            throw new IllegalArgumentException(getText("linkmacro.error.nolinkorname"));
        }
         
        try
        {
            // CONF-44268 - This check is to fix the XSS when customers put a dangerous url into the url field.
            if (!isDangerousURL(url))
            {
                url = "#";
            }
            return handleUrlLink(createUrlLink(text, url), renderContext, url);
        }
        catch (ParseException e)
        {
            return url;
        }
    }
    
    public String handleUrlLink(Link link, RenderContext renderContext, String url)
    {
    	if (link instanceof UnresolvedLink || link instanceof UnpermittedLink)
    		return url;
    	else
    		return linkRenderer.renderLink(link, renderContext);
    }

    protected UrlLink createUrlLink(String text, String url)
            throws ParseException
    {
        return new UrlLink(url, text);
    }
	
	public final boolean hasBody()
    {
        return true;
    }

	public final RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}
	
	public BodyType getBodyType() 
	{
		return BodyType.NONE;
	}
	
	public OutputType getOutputType()
	{
		return OutputType.INLINE;
	}

    private String getEither(Map<String, String> macroParams)
    {
        String result = macroParams.get("0");
        return result == null ? macroParams.get("1") : result;
    }

    private boolean isDangerousURL(String url)
    {
        try
        {
            new URL(url);
            return WebLink.isValidURL(url);
        }
        catch (MalformedURLException ex)
        {
            // Try to decode hex string to find out dangerous url and
            // still keep invalid url but not a dangerous string like "Google", "xyz322iou"
            String decodeUrl = decodeUnicodeEntities(url);
            return WebLink.isValidURL(decodeUrl);
        }
    }

    /**
     * This method is used to decode unicode entities characters that could be rendered by browsers
     * For e.g.: &#x6a;&#x61;&#x76;&#x61;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3a;&#x61;&#x6c;&#x65;&#x72;&#x74;&#x28;&#x31;&#x29 will be rendered as "javascript:alert(1)"
     * @param url unicode entities string
     * @return the normal text.
     */
    private String decodeUnicodeEntities(String url) {
        if (url.indexOf("&#x") > -1)
        {
            String[] temp = url.split("&#x");
            StringBuilder out = new StringBuilder(Math.max(16, url.length()));
            for (int i = 0; i < temp.length; i++) {
                if (!"".equals(temp[i])) {
                    out.append((char) Integer.parseInt(temp[i].replace(";", ""), 16));
                }
            }
            return out.toString();
        }
        else
        {
            return url;
        }
    }
	
	@Override
	public TokenType getTokenType(Map parameters, String body, RenderContext context) 
    {
		return TokenType.INLINE;
	}
	
}

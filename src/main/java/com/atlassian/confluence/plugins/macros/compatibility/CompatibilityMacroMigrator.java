package com.atlassian.confluence.plugins.macros.compatibility;

import java.util.HashMap;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;

public abstract class CompatibilityMacroMigrator implements MacroMigration
{
	
	protected abstract String getDefaultParamName();

	public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
	{
		final String param = macroDefinition.getDefaultParameterValue();
		if (StringUtils.isNotBlank(param))
		{
			macroDefinition.setParameters(
					new HashMap<String, String>(macroDefinition.getParameters())
					{
						{ put(getDefaultParamName(), StringUtils.strip(param)); }
					}
				);
		}
		return macroDefinition;
	}

}

package com.atlassian.confluence.plugins.macros.compatibility;

import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.radeox.util.Encoder;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.links.LinkRenderer;
import com.atlassian.renderer.links.UrlLink;
import com.atlassian.renderer.v2.macro.MacroException;

public class LinkMacroTestCase extends TestCase
{
	private Map<String, String> macroParameters;

    @Mock
    private LinkMacro linkMacro;

    @Mock
    private LinkRenderer linkRenderer;
    
    @Mock
    private PageContext pageContext;
    
    @Mock
    private Page pageToBeRendered;
    
    @Mock
    private ConversionContext conversionContext;

    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);

        macroParameters = new HashMap<String, String>();
        
        linkMacro = new LinkMacro()
        {
            protected String getText(String key)
            {
                return key;
            }
        };
        linkMacro.setLinkRenderer(linkRenderer);
        pageToBeRendered = new Page();
        pageContext = pageToBeRendered.toPageContext();
        conversionContext = new DefaultConversionContext(pageContext);
    }

    public void testIllegalArgumentExceptionThrownIfMacroDoesNotHaveAnyParameters() throws MacroException
    {
        try
        {
            linkMacro.execute(macroParameters, null, conversionContext);
            fail("IllegalArgumentException should be raised if macro has no parameters.");
        }
        catch (final IllegalArgumentException iae)
        {
            /* Succcess */
        }
    }

    public void testLinkRenderedWithNameAndUrl() throws MacroException
    {
        final String dummyValueIndicatingLinkRendered = "UrlFilter.handleUrlLink called";
        UrlLink link = new UrlLink("http://www.google.com/", "Google");
        
        macroParameters.put("text", "Google");
        macroParameters.put("url", "http://www.google.com/");
        
        when(linkRenderer.renderLink(link , pageContext)).thenReturn(dummyValueIndicatingLinkRendered);

        assertEquals(dummyValueIndicatingLinkRendered, linkMacro.execute(macroParameters, null, conversionContext));
    }

    public void testLinkRenderedWithEncodedNameAsUrl() throws MacroException
    {
        final String name = "Google";
        final String dummyValueIndicatingLinkRendered = "UrlFilter.handleUrlLink called";
        final StringBuffer createLinkCalled = new StringBuffer();
        UrlLink link = new UrlLink(name, name);
        macroParameters.put("text", name);
        
        linkMacro = new LinkMacro()
        {
        	protected UrlLink createUrlLink(String text, String url) throws ParseException
            {
                assertEquals(Encoder.toEntity(name.charAt(0)) + name.substring(1), text);
                assertEquals(name, url);
                createLinkCalled.append(Boolean.TRUE);
                return super.createUrlLink(text, url);
            }
        };
        linkMacro.setLinkRenderer(linkRenderer);
        
        
        when(linkRenderer.renderLink(link, pageContext)).thenReturn(dummyValueIndicatingLinkRendered);
        
        assertEquals(dummyValueIndicatingLinkRendered, linkMacro.execute(macroParameters, null, conversionContext));
        assertTrue(Boolean.valueOf(createLinkCalled.toString()).booleanValue()); 

    }

    public void testUrlWrittenToWriterIfLinkCannotBeParsed() throws MacroException
    {
        final String url = "http://www.google.com/";
        
        macroParameters.put("text", "Google");
        macroParameters.put("url", url);

        linkMacro = new LinkMacro()
        {
            protected UrlLink createUrlLink(String text, String url) throws ParseException
            {
                throw new ParseException("Fake ParseException", 0);
            }
        };
        linkMacro.setLinkRenderer(linkRenderer);
        
        assertEquals(url, linkMacro.execute(macroParameters, null, conversionContext));
    }

}

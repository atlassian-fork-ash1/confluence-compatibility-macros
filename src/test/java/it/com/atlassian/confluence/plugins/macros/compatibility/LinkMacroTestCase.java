package it.com.atlassian.confluence.plugins.macros.compatibility;


public class LinkMacroTestCase extends AbstractCompatibilityMacroTestCase
{
	private final static String ALIAS = "foobar";
	private final static String URL = "http://localhost/confluence";
	
	// {link:foobar}
	// {link:foobar|http://localhost/confluence}
	// {link:foobar|url=http://localhost/confluence}
	// {link:http://localhost/confluence|text=foobar}
	// {link:text=foobar}
	// {link:text=foobar|http://localhost/confluence}
	// {link:text=foobar|url=http://localhost/confluence}
	
    public void testRenderTargetAsLinkText()
    {
        viewPage(createPage(testSpaceKey, "testRenderTargetAsLinkText", "{link:http://localhost/confluence}"));
        
        assertEquals(
         		URL, 
         		getElementTextByXPath("//div[@class='wiki-content']/p/a[@href='" + URL + "']")
         );
    }
    
    public void testRenderTargetWithAlias()
    {
        viewPage(createPage(testSpaceKey, "testRenderTargetWithAlias", "{link:http://localhost/confluence|text=foobar}"));
        
        assertLinkMacro();
    }
    
    public void testRenderTargetWithTwoNamelessParameters()
    {
    	viewPage(createPage(testSpaceKey, "testRenderTargetWithTwoNamelessParameters", "{link:foobar|http://localhost/confluence}"));
    	
    	assertLinkMacro();
    }
    
    public void testRenderTargetWithUrlParameter()
    {
    	viewPage(createPage(testSpaceKey, "testRenderTargetWithUrlParameter", "{link:foobar|url=http://localhost/confluence}"));
    	
    	assertLinkMacro();

        viewPage(createPage(testSpaceKey, "testRenderTargetWithUrlParameter - 2", "{link:url=http://localhost/confluence|foobar}"));

        assertLinkMacro();
    }
    
    public void testRenderTargetWithTextParameters()
    {
    	viewPage(createPage(testSpaceKey, "testRenderTargetWithTextAsParameters", "{link:http://localhost/confluence|text=foobar}"));
    	
    	assertLinkMacro();
        
        viewPage(createPage(testSpaceKey, "testRenderTargetWithTextAsParameters - 2", "{link:text=foobar|http://localhost/confluence}"));
        
        assertLinkMacro();
    }
    
    public void testRenderTargetWithOnlyTextParameter()
    {
    	viewPage(createPage(testSpaceKey, "testRenderTargetWithOnlyTextParameter", "{link:text=foobar}"));

    	assertEquals(
         		ALIAS, 
         		getElementTextByXPath("//div[@class='wiki-content']/p/a[@href='" + ALIAS + "']")
        );
    }
    
    public void testRenderTargetWithTextAndUrlParameters()
    {
    	viewPage(createPage(testSpaceKey, "testRenderTargetWithTextAndUrlParameters", "{link:text=foobar|url=http://localhost/confluence}"));
    	
    	assertLinkMacro();
    }

    public void testRenderUrlXSS()
    {
        viewPage(createPage(testSpaceKey, "testRenderUrlXSS", "{link:text=testRenderUrlXSS|url=javascript:alert(document.cookie)}"));

        assertEquals("#", getElementAttributeByXPath("//div[@class='wiki-content']/p/a[@class='external-link']", "href"));
    }

    public void testRenderUrlXSSWithUnicodeEntities()
    {
        viewPage(createPage(testSpaceKey, "testRenderUrlXSS", "{link:text=testRenderUrlXSS|url=&#x6a;&#x61;&#x76;&#x61;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3a;&#x61;&#x6c;&#x65;&#x72;&#x74;&#x28;&#x31;&#x29;}"));

        assertEquals("#", getElementAttributeByXPath("//div[@class='wiki-content']/p/a[@class='external-link']", "href"));
    }

    private void assertLinkMacro()
    {
         assertEquals(
         		ALIAS, 
         		getElementTextByXPath("//div[@class='wiki-content']/p/a[@href='" + URL + "']")
         );
    }
}


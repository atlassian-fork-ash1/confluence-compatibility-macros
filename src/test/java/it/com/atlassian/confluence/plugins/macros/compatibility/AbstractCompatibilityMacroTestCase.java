package it.com.atlassian.confluence.plugins.macros.compatibility;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.util.Date;
import java.util.Arrays;


public class AbstractCompatibilityMacroTestCase extends AbstractConfluencePluginWebTestCase
{

    protected String testSpaceKey;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        createTestSpace();

        try
        {
            gotoPageWithEscalatedPrivileges("/admin/viewplugins.action");
            clickLinkWithText("Compatibility Macros");

            if (getDialog().hasElementByXPath("//a[text()='Enable plugin']"))
                clickLinkWithText("Enable plugin");
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    private void createTestSpace()
    {
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(testSpaceKey = "tst");
        spaceHelper.setName(testSpaceKey);
        spaceHelper.setDescription(testSpaceKey);

        assertTrue(spaceHelper.create());
    }

    protected long createPage(String spaceKey, String pageTitle, String wikiMarkup, String ... labels)
    {
        PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);
        pageHelper.setContent(wikiMarkup);
        pageHelper.setLabels(Arrays.asList(labels));

        assertTrue(pageHelper.create());

        return pageHelper.getId();
    }

    protected long createBlogPost(String spaceKey, String blogTitle, String wikiMarkup, Date postDate, String ... labels)
    {
        BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey(spaceKey);
        blogPostHelper.setTitle(blogTitle);
        
        blogPostHelper.setContent(wikiMarkup);
        blogPostHelper.setCreationDate(postDate);

        blogPostHelper.setLabels(Arrays.asList(labels));

        assertTrue(blogPostHelper.create());

        return blogPostHelper.getId();
    }

    protected void viewPage(long pageId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + pageId);
    }
}

package it.com.atlassian.confluence.plugins.macros.compatibility;

import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.apache.commons.lang.time.FastDateFormat;

import java.util.Date;
import java.util.regex.Pattern;

public class BlogPostsMacroTestCase extends AbstractCompatibilityMacroTestCase
{
    private void assertBlogPostRowInTitlesMode(int row, String blogPostTitle, String authorName, Date postDate)
    {

        assertEquals(
                blogPostTitle,
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (row + 1) + "]/td/a")
        );

        UserHelper userHelper = getUserHelper(authorName);
        assertTrue(userHelper.read());
        assertEquals(
                userHelper.getFullName(),
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (row + 1) + "]/td[2]/a")
        );


        assertEquals(
                FastDateFormat.getInstance("MMM dd, yyyy").format(postDate),
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (row + 1) + "]/td[3]")
        );
    }

    private void assertBlogPostTableHeadersInTitlesMode()
    {
        assertEquals("Title", getElementTextByXPath("//div[@class='wiki-content']/table//tr/th"));
        assertEquals("Author", getElementTextByXPath("//div[@class='wiki-content']/table//tr/th[2]"));
        assertEquals("Date Posted", getElementTextByXPath("//div[@class='wiki-content']/table//tr/th[3]"));
    }

    private void sleep(long millis) throws InterruptedException
    {
        Thread.sleep(millis);
    }

    public void testRenderBlogPostTitlesOnly() throws InterruptedException
    {
        Date postDate1;
        Date postDate2;
        Date postDate3;

        createBlogPost(testSpaceKey, "Blog 1", "Blog 1 body", postDate1 = new Date(), "blog1");

        sleep(500);
        createBlogPost(testSpaceKey, "Blog 2", "Blog 2 body", postDate2 = new Date(), "blog2");

        sleep(500);
        createBlogPost(testSpaceKey, "Blog 3", "Blog 3 body", postDate3 = new Date(), "blog3");

        long testPageId = createPage(testSpaceKey, "testRenderBlogPostTitlesOnly", "{weblogs:content=titles}");
        
        viewPage(testPageId);

        assertBlogPostTableHeadersInTitlesMode();

        String currentUserName = getConfluenceWebTester().getCurrentUserName();

        assertBlogPostRowInTitlesMode(1, "Blog 3", currentUserName, postDate3);
        assertBlogPostRowInTitlesMode(2, "Blog 2", currentUserName, postDate2);
        assertBlogPostRowInTitlesMode(3, "Blog 1", currentUserName, postDate1);
        
    }

    private void assertBlogPostInNormalMode(int position, String blogTitle, Date postDate, String expectedContent,  String author, int commentCount, boolean canEdit, String ... labels)
    {
//        assertEquals(
//                FastDateFormat.getInstance("EEEE, MMMM d, yyyy").format(postDate),
//                getElementTextByXPath("//div[" + position + "]/a[@class='blogDate']")
//        );

        assertEquals(
                blogTitle,
                getElementTextByXPath("//div[@class='blogpost'][" + position + "]//a[@class='blogHeading']")
        );

        UserHelper userHelper = getUserHelper(author);
        assertTrue(userHelper.read());

        assertEquals(
                userHelper.getFullName(),
                getElementTextByXPath("//div[@class='blogpost'][" + position + "]/div[@class='pagesubheading']/a")
        );

        String lastModifiedString = getElementTextByXPath("//div[@class='blogpost'][" + position + "]/div[@class='pagesubheading']");
        
        assertTrue(
                Pattern.compile(
                        "Last\\s+changed\\s+\\Q" + FastDateFormat.getInstance("MMM dd, yyyy").format(postDate) + " \\E\\d+:\\d+\\s+by\\s+" + userHelper.getFullName() + ".*",
                        Pattern.DOTALL
                ).matcher(
                        lastModifiedString
                ).matches()
        );

        for (String label : labels)
            assertElementPresentByXPath("//div[@class='blogpost'][" + position + "]/div[@class='pagesubheading']/a[text()='" + label + "']");

        assertEquals(
                expectedContent,
                getElementTextByXPath("//div[@class='blogpost'][" + position + "]/div[@class='wiki-content']")
        );

        assertEquals(
                commentCount + " comment" + (commentCount == 1 ? "" : "s"),
                getElementTextByXPath("//div[@class='blogpost'][" + position + "]/div[@class='endsection']/a[2]") 
        );

        if (canEdit)
            assertElementPresentByXPath("//div[@class='blogpost'][" + position + "]/div[@class='endsection']/a[3]");
        else
            assertElementNotPresentByXPath("//div[@class='blogpost'][" + position + "]/div[@class='endsection']/a[3]");
    }

    private long createComment(long contentId, String comment)
    {
        CommentHelper commentHelper = getCommentHelper();

        commentHelper.setContentId(contentId);
        commentHelper.setContent(comment);

        assertTrue(commentHelper.create());

        return commentHelper.getId();
    }

    public void testRenderBlogPostsWithExcerpts() throws Exception
    {
        Date postDate1;
        Date postDate2;
        Date postDate3;

        viewPage(createBlogPost(testSpaceKey, "Blog 1", "Blog 1 body", postDate1 = new Date(), "blog1", "blog0"));

        sleep(500);
        long blogPostId2 = createBlogPost(testSpaceKey, "Blog 2", "{excerpt:hidden=true}Excerpt of blog 2{excerpt}Blog 2 body", postDate2 = new Date(), "blog2");

        createComment(blogPostId2, "A comment");
        viewPage(blogPostId2);

        sleep(500);
        viewPage(createBlogPost(testSpaceKey, "Blog 3", "{excerpt:hidden=true}Excerpt of blog 3{excerpt}Blog 3 body", postDate3 = new Date(), "blog3"));

        long testPageId = createPage(testSpaceKey, "testRenderBlogPostsWithExcerpts", "{weblogs:content=excerpts}");

        viewPage(testPageId);

        String currentUserName = getConfluenceWebTester().getCurrentUserName();

        assertBlogPostInNormalMode(1, "Blog 3", postDate3, "Excerpt of blog 3", currentUserName, 0, true, "blog3");
        assertBlogPostInNormalMode(2, "Blog 2", postDate2, "Excerpt of blog 2", currentUserName, 1, true, "blog2");
        assertBlogPostInNormalMode(3, "Blog 1", postDate1, "Blog 1 body", currentUserName, 0, true, "blog1", "blog0");
        
    }
}
